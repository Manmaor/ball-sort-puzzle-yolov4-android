
import cv2

def get_class_by_color(rgb):
    '''
    classes:
        -1 empty 
        0  pink				(123, 94, 234)
        1  grey				(102, 100, 99)
        2  red				(35, 43, 197)
        3  orange			(65, 140, 232)
        4  purple			(147, 43, 114)
        5  brown			(8, 74, 125)
        6  yellow			(88, 218, 241)

        7  dark green		(51, 101, 17)
        8  green		    (14, 151, 120)
        9  light green		(125, 214, 97)

        10 blue				(195, 47, 59)
        11 light blue		(229, 163, 85)
        12 group
    '''
    try:
        return {
            (123, 94, 234)  : (0  ,'pink'),
            (102, 100, 99)  : (1  ,'grey'),
            (35, 43, 197)   : (2  ,'red'),
            (65, 140, 232)  : (3  ,'orange'),
            (147, 43, 114)  : (4  ,'purple'),
            (8, 74, 125)    : (5  ,'brown'),
            (88, 218, 241)  : (6  ,'yellow'),
            (51, 101, 17)   : (7  ,'dark green'),
            (14, 151, 120)  : (8  ,'green'),
            (125, 214, 97)  : (9  ,'light green'),
            (195, 47, 59)   : (10 ,'blue'),
            (229, 163, 85)  : (11 ,'light blue')
        }[rgb]
    except:
        return (-1, 'empty')

def translate_can(img, top_x, top_y):
    width = 67
    height = 67

    jump_down = 67

    img_height, img_width, channels = img.shape

    # draw group
    # cv2.rectangle(
    #         img,
    #         (top_x-20, top_y-45),
    #         (top_x+width+20, (top_y+height+ jump_down*3)+10), #-i
    #         (255,0,0))

    group_top_left = (top_x-20, top_y-45)
    group_bottom_right = (top_x+width+20, (top_y+height+ jump_down*3)+10)
    group_dim = (group_bottom_right[0] - group_top_left[0], group_bottom_right[1] - group_top_left[1]) # width, height
    group_center = (group_top_left[0]+group_dim[0]/2, group_top_left[1]+group_dim[1]/2)
    group_normal_center = (group_center[0]/img_width, group_center[1]/img_height)
    group_normal_dim = (group_dim[0]/img_width, group_dim[1]/img_height)

    yield f'12 {group_normal_center[0]} {group_normal_center[1]} {group_normal_dim[0]} {group_normal_dim[1]}'

    for i in range(4):
        # draw all (even empty)
        # cv2.rectangle(
        #     img,
        #     (top_x, (top_y+jump_down*i)),
        #     (top_x+width, (top_y+height + jump_down*i)), #-i
        #     (255,255,255))
        
        x_center = (top_x+width/2)
        y_center = ((top_y+jump_down*i)+(height/2))
        nornal_x_center = x_center / img_width
        nornal_y_center = y_center / img_height
        nornal_width = width / img_width
        nornal_height = height / img_height

        color = get_class_by_color((img[int(y_center), int(x_center), 0], img[int(y_center), int(x_center), 1], img[int(y_center), int(x_center), 2]))
        if color[0] != -1:
            # draw only balls
            # cv2.rectangle(
            #     img,
            #     (top_x, (top_y+jump_down*i)),
            #     (top_x+width, (top_y+height + jump_down*i)), #-i
            #     (255,255,255))
            yield f'{color[0]} {nornal_x_center} {nornal_y_center} {nornal_width} {nornal_height}'

def translate_image_to_data(img, is_odd):
    x_start = 26
    y_start = 483
    top_rows = 6
    bottom_rows = 3+2
    jump_right = 120
    big_jump_side = 60
    big_jump_down = 199

    if is_odd:
        x_start = 9
        top_rows = 7
        bottom_rows = 5+2
        jump_right = 106
        big_jump_side = 0

    for i in range(top_rows):
        yield translate_can(img, x_start+jump_right*i, y_start)

    for i in range(bottom_rows):
        yield translate_can(img, x_start+jump_right*i+big_jump_side, y_start+(67*3)+big_jump_down)




imgs_path = 'data/test/'

import os

with os.scandir(imgs_path) as entries:
    for entry in entries:
        file_name, file_type = entry.name.split('.')
        if file_type != 'jpeg':
            continue

        img = cv2.imread(entry.path)
        is_odd = int(file_name)%2
        
        with open(os.path.join(imgs_path, file_name+'.txt'), 'w') as f:
            for can_data in translate_image_to_data(img, is_odd):
                for ball_data in can_data:
                        f.write(ball_data+'\n')
                        # pass # 2
        
        print(f'{file_name}')
        # cv2.imshow('frame', img) # 3
        # cv2.waitKey() # 4

        # break # 1