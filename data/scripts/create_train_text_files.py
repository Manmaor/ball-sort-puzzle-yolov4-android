import argparse
import os

parser = argparse.ArgumentParser(description='Create images txt file.')
parser.add_argument('dir', type=str,
                    help='the image directory')
parser.add_argument('--type', type=str,
                    help='the image type (default jpeg)', default='jpeg')
parser.add_argument('--output-filename', dest='output', type=str,
                    help='the output file to save the names (default using dir arg)')
parser.add_argument('--root-dir', dest='root_dir', type=str,
                    help='a prefix destination to the filename')

args = parser.parse_args()
# print(args.accumulate(args.integers))

output_file = args.dir + '.txt'
if args.output:
    output_file = args.output


with os.scandir(args.dir) as entries:
    with open(output_file, 'w') as f:
        for entry in entries:
            file_name, file_type = entry.name.split('.')
            if file_type == args.type:
                to_write = os.path.join(args.dir, entry.name)
                if args.root_dir:
                    to_write = os.path.join(args.root_dir, to_write)

                f.write(to_write.replace(os.path.sep, '/')+'\n')