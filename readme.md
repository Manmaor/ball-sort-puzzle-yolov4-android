# Credits :)
* https://www.paperspace.com/ - for gpu server
* https://github.com/AlexeyAB/darknet - YOLO
* https://github.com/Tianxiaomo/pytorch-YOLOv4 - not necessary
* https://github.com/Tencent/ncnn - yolov4 to ncnn
* https://github.com/dog-qiuqiu/Android_NCNN_yolov4-tiny - application with yoloV4 tiny implemented using ncnn
* https://www.youtube.com/watch?v=RdO3xMgoLr4 - yolov4 cfg and weights to ncnn
* https://www.youtube.com/watch?v=82HUOgszlaE - build application with ncnn
* https://github.com/akcio/ball_sort_puzzle_solver - ball sort puzzle algo


# How to
1. create server in https://www.paperspace.com/
2. pull and build https://github.com/AlexeyAB/darknet
3. train on your data
   1. yes you need to prepare data before (details in the darknet link)
4. pull and build https://github.com/Tencent/ncnn
   1. run `./darknet2ncnn yolov4-tiny.cfg yolov4-tiny.weights yolov4-tiny.param yolov4-tiny.bin 1`
   2. run `./ncnnoptimize yolov4-tiny.param yolov4-tiny.bin yolov4-tiny-opt.param yolov4-tiny-opt.bin 0`
5. pull application
   1. replace `yolov4-tiny-opt.param` and `yolov4-tiny-opt.bin` in the app
   2. change the classes in `Yolov5.h` and `Box.java`
   3. edit and have fun
