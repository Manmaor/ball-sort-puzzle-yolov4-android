package gd.hq.yolov5.rv

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import gd.hq.yolov5.R
import gd.hq.yolov5.solver.State

class VisualStepsAdapter(private val dataSet: List<State>):
        RecyclerView.Adapter<VisualStepsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val imageView: ImageView = view.findViewById(R.id.img)

        fun bind(image: Bitmap) {
            imageView.setImageBitmap(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.rv_image_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataSet[position].drawImage())
    }

    override fun getItemCount(): Int = dataSet.size
}