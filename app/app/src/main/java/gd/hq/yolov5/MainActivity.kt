package gd.hq.yolov5

//import gd.hq.yolov5.MainActivity.DetectAnalyzer
import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraX
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import gd.hq.yolov5.rv.TextStepsAdapter
import gd.hq.yolov5.solver.Ball
import gd.hq.yolov5.solver.Flask
import gd.hq.yolov5.solver.Solver
import gd.hq.yolov5.solver.State
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.lang.Exception
import java.util.*


import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import gd.hq.yolov5.rv.VisualStepsAdapter


class MainActivity : AppCompatActivity() {
    companion object {
        const val FROM_GALLERY_REQUEST_CODE = 6458
        const val OVERLAY_REQUEST_CODE = 5469

        private const val REQUEST_EXTERNAL_STORAGE = 1
        private const val REQUEST_PICK_IMAGE = 2
        private val PERMISSIONS_STORAGE = arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        const val threshold = 0.5
        const val nms_threshold = 0.4
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(my_toolbar)
        val permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            )
        }
        val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:$packageName"))

        if (!Settings.canDrawOverlays(this)) {
            startActivityForResult(intent, OVERLAY_REQUEST_CODE)
        } else {
//            drawOnScreen()
        }

        YOLOv5.init(assets)

        btn_pick_photo.setOnClickListener {
            val pickPhoto = Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(pickPhoto, FROM_GALLERY_REQUEST_CODE)
        }

        recyclerView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(
                this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
    }

    override fun onDestroy() {
        CameraX.unbindAll()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        for (result in grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data == null) {
            return
        }
//        val image = getPicture(data.data)
        val imageUri = data.data
        Picasso.get().load(imageUri).into(object: Target {
            override fun onBitmapLoaded(image: Bitmap, from: Picasso.LoadedFrom?) {
                runYolo(image)
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                Toast.makeText(this@MainActivity, "Failed to load image", Toast.LENGTH_LONG).show()
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
            }

        })

    }

    fun runYolo(image: Bitmap) {
        val result = YOLOv5.detect(image, threshold, nms_threshold)
        processImage(image, yoloResult=result)
        prepSolver(result)
    }

    fun processImage(image: Bitmap, yoloResult: Array<out Box>) {
        val mutableBitmap = image.copy(Bitmap.Config.ARGB_8888, true)
        val canvas = Canvas(mutableBitmap)
        val boxPaint = Paint()
        boxPaint.alpha = 200
        boxPaint.style = Paint.Style.STROKE
        boxPaint.strokeWidth = (4 * image.width / 800).toFloat()
        boxPaint.textSize = (40 * image.width / 800).toFloat()
        for (box in yoloResult) {
            boxPaint.color = box.color
            boxPaint.style = Paint.Style.FILL
            canvas.drawText(box.label, box.x0 + 3, box.y0 + 17, boxPaint)
            boxPaint.style = Paint.Style.STROKE
            canvas.drawRect(box.rect, boxPaint)
        }
//        img_view!!.setImageBitmap(mutableBitmap)

        btn_show_detection.tag = mutableBitmap
        btn_show_detection.visibility = View.VISIBLE
        btn_show_detection.setOnClickListener {
            val dialog_view =
                    this.layoutInflater.inflate(R.layout.dialog_detection_result, null)
            (dialog_view as ImageView).setImageBitmap(it.tag as Bitmap)

            AlertDialog.Builder(this)
                    .setTitle("Detection Result")
                    .setView(dialog_view)
                    .setPositiveButton("Close", DialogInterface.OnClickListener {
                        dialog, which ->
                        dialog.dismiss()
                    }).show()
        }
    }

    fun prepSolver(yoloResult: Array<out Box>) {
        val yoloFlasks = yoloResult
                .filter { it.label == "ball_group" }
                .sortedBy { it.x0 + (it.y0 * 10) }
        val yoloBalls = yoloResult.filter { it.label != "ball_group" }


        val isDetectionFailed = yoloBalls
                .groupBy { it.label }
                .filter { entry: Map.Entry<String, List<Box>> -> entry.value.size != 4 }
                .isNotEmpty()

        if (isDetectionFailed) {
            Toast.makeText(this@MainActivity, "Detection Failed", Toast.LENGTH_LONG).show()
            return
        }

        val solverFlasks = yoloFlasks.map { flask ->
            val containedBalls =
                    yoloBalls.filter { ball ->  isFlaskContainsTopLeft(flask, ball) || isFlaskContainsBottomRight(flask, ball) }
                            .sortedByDescending { it.y0 }
                            .map { Ball.valueOf(it.label.toUpperCase()) }

            Flask(4, *containedBalls.toTypedArray())
        }

        val startState = State(solverFlasks)
        val solvedState = Solver.solve(startState, this, "0")

        solvedState?.getSteps()?.reversed()?.let {
            recyclerView.adapter = TextStepsAdapter(it)
        } ?: Toast.makeText(this@MainActivity, "No possible solution", Toast.LENGTH_LONG).show()

        solvedState?.let {
            btn_show_visual_steps.tag = it.asList().reversed()
            btn_show_visual_steps.visibility = View.VISIBLE
            btn_show_visual_steps.setOnClickListener {
                val dialog_view =
                        this.layoutInflater.inflate(R.layout.dialog_visual_steps, null) as RecyclerView
                dialog_view.setHasFixedSize(true)
                val layoutManager = LinearLayoutManager(
                        this, RecyclerView.HORIZONTAL, false)
                dialog_view.layoutManager = layoutManager
                dialog_view.adapter = VisualStepsAdapter(btn_show_visual_steps.tag as List<State>)

                AlertDialog.Builder(this)
                        .setTitle("Detection Result")
                        .setView(dialog_view)
                        .setPositiveButton("Close", DialogInterface.OnClickListener {
                            dialog, which ->
                            dialog.dismiss()
                        }).show()
            }
        } ?: { btn_show_visual_steps.visibility = View.GONE }()
    }

    fun isFlaskContainsTopLeft(flask: Box, ball: Box) = (ball.x0 > flask.x0 && ball.x0 < flask.x1) && (ball.y0 > flask.y0 && ball.y0 < flask.y1)
    fun isFlaskContainsBottomRight(flask: Box, ball: Box) = (ball.x1 > flask.x0 && ball.x1 < flask.x1) && (ball.y1 > flask.y0 && ball.y1 < flask.y1)


    fun drawOnScreen() {
        val v = LayoutInflater.from(this).inflate(R.layout.text_overlay, null)

        val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.addView(v, v.layoutParams)

    }

}