package gd.hq.yolov5.solver

import android.content.Context

object Solver {
    fun solve(state: State, c: Context, name: String): State? {
        if (state.isWinningState()) {
//            state.save(c, "$name.Win")
            return state
        }

//        state.save(c, name)

        state.flasks.forEach flasksLoop@{ currentFlask ->
            // empty, 3OfTheSame, 4OfTheSame
            if (currentFlask.isEmpty() ||
                    (currentFlask.isFull() && currentFlask.isOneColor()) ||
                    currentFlask.isNeedOneMore())
                return@flasksLoop

            val currentBall = currentFlask.balls.last()
            state.flasks.forEachIndexed placableLoop@{ index, placableFlask ->
                if (placableFlask == currentFlask || placableFlask.isFull()) {
                    return@placableLoop
                }

                if (placableFlask.isEmpty() || placableFlask.isLastValueEqual(currentBall)) {
                    val newState = state.move(currentFlask, placableFlask)
                    if (state.isNewState()) {
                        solve(newState, c, "$name.$index")?.let {
                            return it
                        }
                    }

                }
            }
        }

        return null
    }
}