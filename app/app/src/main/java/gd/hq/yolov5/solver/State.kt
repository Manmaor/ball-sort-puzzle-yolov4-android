package gd.hq.yolov5.solver

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Environment

enum class Ball(val str: String) {
    PINK("#ea5e7b"),
    GREY("#636466"),
    RED("#c52b23"),
    ORANGE("#e88c41"),
    PURPLE("#722b93"),
    BROWN("#7d4a08"),
    YELLOW("#f1da58"),
    DARK_GREEN("#116533"),
    GREEN("#78970e"),
    LIGHT_GREEN("#61d67d"),
    BLUE("#3b2fc3"),
    LIGHT_BLUE("#55a3e5"),
}

//ImmutableList
class Flask(val maxCount: Int, vararg val balls: Ball) {

    fun isEmpty() = balls.isEmpty()

    fun isFull() = balls.size >= maxCount

    fun isOneColor() = balls.all { it == this.balls[0] }

    fun isNeedOneMore() = balls.size == (maxCount - 1) && this.isOneColor()

    fun isLastValueEqual(ball: Ball) = balls.last() == ball

    fun isContentSame(otherFlask: Flask): Boolean {
        return otherFlask.balls.contentEquals(this.balls)
    }

    override fun toString(): String {
        return "${balls.map{ it.toString() }}"
    }

//    override fun hashCode(): Int {
//        var result = maxCount
//        result = 31 * result + balls.contentHashCode()
//        return result
//    }
}

class State(
        val flasks: List<Flask>,
        private val groupSize: Int = 4,
        private val parentState: State? = null,
        private val toFlask: Flask? = null,
        private val fromFlask: Flask? = null,

        ) {
    private val groupsCount: Int = flasks.size

    fun isWinningState() = flasks.all { (it.isFull() && it.isOneColor()) || it.isEmpty()  }

    fun move(fromFlask: Flask, toFlask: Flask): State {
        val ball = fromFlask.balls.last()
        val newFromFlask = Flask(fromFlask.maxCount, *fromFlask.balls.dropLast(1).toTypedArray())
        val newToFlask = Flask(toFlask.maxCount, *toFlask.balls, ball)

        val newFlasks = flasks.map {
            when (it) {
                fromFlask -> newFromFlask
                toFlask -> newToFlask
                else -> it
            }
        }

        return State(flasks = newFlasks, parentState = this, fromFlask = newFromFlask, toFlask = newToFlask)
    }

    fun isNewState(): Boolean {
        var state = this.parentState

        while (state != null) {
            if (this == state)
                return false
            state = state.parentState
        }

        return true
    }

    // Could be optimized (a lot)
    override fun equals(other: Any?): Boolean {
        if (other !is State)
            return false

        var otherFlasks = other.flasks.toMutableList()
        this.flasks.forEach { flask ->
            val filtered = otherFlasks.filter { it.isContentSame(flask) }
            if (filtered.isNotEmpty()) {
                otherFlasks.remove(filtered[0])
            }
        }

        if (otherFlasks.isEmpty())
            return true
        return false
    }

    fun getSteps(): ArrayList<String> {
        val list = ArrayList<String>()

        var currentState: State? = this
        while (currentState!!.parentState != null) {
            val ball = currentState.toFlask!!.balls.last()
            val indexFrom = currentState.flasks.indexOf(currentState.fromFlask)+1
            val indexTo = currentState.flasks.indexOf(currentState.toFlask)+1

            list.add("$ball, $indexFrom -> $indexTo")

            currentState = currentState.parentState
        }

        return list
    }

    fun drawImage(): Bitmap {
        val start_x = 10f
        val jump_right = 220f/6f
        val start_y = 20f
        val ball_jump_y = 200f/4f
        val nextRowHeight = 250f

        val bitmap = Bitmap.createBitmap(250, 550, Bitmap.Config.ARGB_8888).copy(Bitmap.Config.ARGB_8888, true)
        val canvas = Canvas(bitmap)
        val paint = Paint()
        paint.alpha = 255
        paint.style = Paint.Style.FILL

        flasks.forEachIndexed { flaskIndex, flask ->
            flask.balls.forEachIndexed() { ballIndex, ball ->
                var isNextRow = 0
                if (flaskIndex > 5)
                    isNextRow = 1

                paint.color = Color.parseColor(ball.str)
                canvas.drawCircle(
                        start_x + (flaskIndex % 6) * jump_right,
                        start_y + (nextRowHeight * isNextRow) + ( (ball_jump_y*3)  - (ball_jump_y*ballIndex) ),
                        10.0f,
                        paint)
            }
        }

        return bitmap
//        val fileOutputStream = c.openFileOutput("$name.PNG", Context.MODE_PRIVATE)
//        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
//        fileOutputStream.close()
    }

    fun asList(): List<State> {
        val steps = ArrayList<State>()

        var currentState: State? = this
        while (currentState != null) {
            steps.add(currentState)
            currentState = currentState.parentState
        }

        return steps
    }

}