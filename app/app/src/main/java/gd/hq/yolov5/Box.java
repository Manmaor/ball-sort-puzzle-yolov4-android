package gd.hq.yolov5;

import android.graphics.Color;
import android.graphics.RectF;

import java.util.Random;

public class Box {
    public float x0,y0,x1,y1;
    private int label;
    private float score;
    private RectF rect;
    private static String[] labels={
            "pink",
            "grey",
            "red",
            "orange",
            "purple",
            "brown",
            "yellow",
            "dark_green",
            "green",
            "light_green",
            "blue",
            "light_blue",
            "ball_group"
    };
    public Box(float x0,float y0, float x1, float y1, int label, float score){
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
        this.label = label;
        this.score = score;
        this.rect = new RectF(x0,y0,x1,y1);
    }

    public RectF getRect(){
        return this.rect;
    }

    public String getLabel(){
        return labels[label];
    }

    public float getScore(){
        return score;
    }

    public int getColor(){
        Random random = new Random(label);
        return Color.argb(255,random.nextInt(256),random.nextInt(256),random.nextInt(256));
    }
}
